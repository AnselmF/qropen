#!/usr/bin/python3
"""
A one-module QR code scanner.

Start it and hold something with a QR code into the camera the thing has
opened (which is OpenCV's default one).  You'll be asked whether to
open the URL with your default browser.

You can also pass in an image you've taken before.

Dependencies (Debian): python3-numpy, python3-zbar, python3-opencv.

Distributed unter CC0.
"""

import sys
import threading
import tkinter
import webbrowser

import cv2
import numpy
import zbar


XRES, YRES = 640, 480
#XRES, YRES = 1280, 720


class Webbutton(tkinter.Tk):
	"""A little Tkinter UI for opening a URI.
	"""
	def __init__(self, uri):
		tkinter.Tk.__init__(self)
		self.uri = uri
		tkinter.Button(self, text="Open\n"+uri, command=self._do_open
			).pack(expand=True, fill=tkinter.BOTH, side=tkinter.LEFT)
		tkinter.Button(self, text="Cancel", command=self._close
			).pack(expand=True, fill=tkinter.BOTH, side=tkinter.LEFT)
	
	def _do_open(self):
		self._close()
		# my webbrowser doesn't properly detach.  I'm collecting dead threads
		# by cheating around it for the time being, but I don't thing this
		# will open too many URLs.
		t = threading.Thread(
			target=lambda: webbrowser.open(self.uri), daemon=True)
		t.start()

	def _close(self):
		self.withdraw()
		self.quit()


def _normalizeForImage(pixels, gamma):
	"""returns pixels normalised and gamma-graded.
	"""
	pixMax, pixMin = numpy.max(pixels), numpy.min(pixels)
	return numpy.asarray(numpy.power(
		(pixels-pixMin)/(pixMax-pixMin), gamma)*255, 'uint8')


def grab_image(cap, centre=(XRES//2, YRES//2), sub_size=(XRES//6, YRES//4)):
	"""obtains an impage from cap and returns it as a numpy array.
	"""
	img_available, img = cap.read()
	if not img_available:
		raise Exception("Could not take photo")

	img = cv2.getRectSubPix(img, sub_size, centre)
	img = cv2.resize(img, (sub_size[0]*2, sub_size[1]*2))
	unsharp_mask = cv2.GaussianBlur(img, (7, 7), 4)
	img = cv2.addWeighted(img, 4, unsharp_mask, -3, 0)
	return numpy.asarray(_normalizeForImage(
		0.21*img[:,:,0]+0.72*img[:,:,1]+0.07*img[:,:,2], 1.4), 'uint8')


def open_codes(img):
	"""opens links found as qr codes within the PIL image img.
	"""
	image = zbar.Image(
		img.shape[1], img.shape[0], 'Y800', data=img.tobytes())

	scanner = zbar.ImageScanner()
	scanner.scan(image)

	for item in image:
		if item.type==item.QRCODE:
			payload = item.data
			if payload.startswith("www"):
				payload = "http://"+payload
			if payload.startswith("http"):
				Webbutton(payload).mainloop()
				continue
			else:
				print("Non-URL:", payload)
		else:
			print("Unhandled non-QR:", item.data)


def main():
	if len(sys.argv)==2:
		img = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)
		open_codes(img)
		return

	cap = cv2.VideoCapture(0)
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, XRES)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, YRES)

	while True:
		img = grab_image(cap)
		cv2.imshow("Looking for QR code", img)
		
		open_codes(img)

		key = cv2.waitKey(20)
		if key==113: # q
			break


if __name__=="__main__":
	main()
