=========
qropen.py
=========

If you are a smartphone-less person such as me, have you ever cursed
people who put QR codes somewhere but no readable URIs?  Well, this
little script to the rescue.  This is a QR code scanner for ordinary
computers that have a camera.

Installation
------------

Install the dependencies, copy qropen.py into your path (under whatever
name), make it executable, done.


Dependencies
------------

By Debian package:

* python3-numpy
* python3-zbar
* python3-opencv


Usage
-----

Call the script without an argument and it will show a camera image in a
window.  Use that to position a QR code until the program recognises it.
At that point, you'll be asked whether to open a URL if that's what's
in there.  Other stuff (that might well be in a QR code or an EAN code
or whatever) will be dumped to stdout.  Hence, it's a good idea to
start this on a terminal).

You can also pass in an image on the command line; there is a real
chance the program will find embedded QR (or other bar-) codes.

In principle, the program should also decode all kind of other bar
codes, in particular EAN.  On a camera as in the Thinkpad X240, however,
that does not work very well since it blurs out the thin likes in EAN
codes too much.


Hacking
-------

This is a mere 120 lines, because all the heavy lifting is done by zbar
and opencv.  Still, there's a lot you may want (or need?) to tweak.

* ``XRES`` and ``YRES``, the size of the video taken with the camera.  I
  have experimented with higher resolutions, but since it's mainly light
  and focus that's the problem here in typical situations, that did not
  help (on a Thinkpad X240).

* sub_size – this is defined in ``grab_image``'s arguments to be some
  fixed fraction of the full image coming from the camera.  I'm cropping
  that image because zbar won't recognise typical symbologies on images
  with lots of pixels anyway.  Of course, it also saves CPU.  But
  perhaps you'd like to process more pixels anyway.

* unsharp masking – in ``grab_image``, I'm doing unsharp masking (i.e.,
  image processing to sharpen the image) in order to compensate for the
  lack of focus in the camera.  Most QR codes are so small that you have
  to hold them so close to the camera that the fix-focus optics built
  into your typical computer only yield a rather blurry image.  You can
  try to fiddle around with the size of the mask (the argument of 
  ``GaussianBlur``) or perhaps the overlay of the unsharp mask (the
  arguments of ``addWeighted``).

* The gamma value – the last argument to ``_normalizeForImage`` is a
  `gamma`_.  Roughly, increase that number to have “less gray” in the
  image and more black and white.  The mixing of the various colour
  channels in in that function probably is only interesting for you if
  you have really nasty QR codes that somehow abuse colour.

.. _gamma: https://en.wikipedia.org/wiki/Gamma_correction



Licence
-------

None, because I don't even want to claim copyright for this.

In other words: This is distributed under CC0_.

.. _CC0: http://creativecommons.org/publicdomain/zero/1.0/
